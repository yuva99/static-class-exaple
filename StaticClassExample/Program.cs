﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticClassExample
{
    class Program
    {
        static void Main(string[] args)
        {
            MyNonStaticClass.MyStaticMethod();
        }
    }

    public static class MyStaticClass
    {
        public static int myStaticVariable = 0;

        public static void MyStaticMethod()
        {
            Console.WriteLine("This is a static method.");

            //A static constructor in a static class runs only once when any of its static members accessed for the first time.
        }

        public static int MyStaticProperty { get; set; }

        //static MyStaticClass(int a)
        //{

        //}
        //public int intpaint = 0;
        //public void Circle()
        //{


        //}

        //u cant declared method/variables/constructor without static keyword
        //static constructor must be parameterless

    }

    public class MyNonStaticClass
    {
        private static int myStaticVariable = 0;

        public static void MyStaticMethod()
        {
            Console.WriteLine("This is static method.");
        }
        static MyNonStaticClass()
        {
            //always call first constructor
            //A static constructor in a static class runs only once when any of its static members accessed for the first time.
        }
        //always call first constructor
        //MyNonStaticClass()
        //{


        //}

        MyNonStaticClass(int a)
        {

        }
        private MyNonStaticClass()
        {
            //private constructor work when parameter and static constructor
        }
        public void myNonStaticMethod()
        {
            Console.WriteLine("Non-static method");
        }
    }




}
